import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { HomeResolver } from '../resolvers/home-resolver';

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: '',
      component: HomeComponent,
      resolve: {
        homeData: HomeResolver,
      }
    }
  ])],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
