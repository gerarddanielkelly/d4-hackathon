import { Component, ElementRef, ViewChild, AfterViewInit, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import * as crossfilter from 'crossfilter2';
import * as d3 from 'd3';
import * as dc from 'dc';
import { RouterOutlet } from '@angular/router';
import { d4Data } from '../models/data-model';
import { MatSelectionListChange, MatSelectionList } from '@angular/material/list';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, AfterViewInit {
  @ViewChild('list') listRef: MatSelectionList;

  @ViewChild('pieChart') pieChartRef: ElementRef;
  @ViewChild('compositeChart') compositeChartRef: ElementRef;
  @ViewChild('heatmapChart') heatmapChartRef: ElementRef;
  @ViewChild('heatmapLegend') heatmapLegendRef: ElementRef;

  @ViewChild('scoreCardOpen') scoreCardOpenRef: ElementRef;
  @ViewChild('scoreCardEscalated') scoreCardEscalatedRef: ElementRef;
  @ViewChild('scoreCardReviewed') scoreCardReviewedRef: ElementRef;
  @ViewChild('scoreCardClosed') scoreCardClosedRef: ElementRef;
  scoreCardRefs: ElementRef[];

  // Charts
  pieChart: dc.pieChart;
  compositeChart: dc.CompositeChart;
  heatmapChart: dc.HeatMap;
  heatmapLegend: dc.HeatMap;

  // Dimensions
  compositeDimension: crossfilter.Dimension<any, any>;
  statusDimension: crossfilter.Dimension<any, any>;
  statusTypeDimension: crossfilter.Dimension<any, any>;
  listDimension: crossfilter.Dimension<any, any>;
  heatmapDimension: crossfilter.Dimension<any, any>;
  heatmapLegendDimension: crossfilter.Dimension<any, any>;

  // Filter array
  filters: Array<string> = [];

  // Colour map
  colourMap: Array<string> = [
    'red',
    'purple',
    'blue',
    'pink',
    'green'
  ];

  // Days of week mapping
  days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  daysOrder = {
    Monday: 0,
    Tuesday: 1,
    Wednesday: 2,
    Thursday: 3,
    Friday: 4,
    Saturday: 5,
    Sunday: 6,
  };

  // Status
  lastStatusSelected = '';
  notEscalatedStatus = ['open','reviewed','closed'];

  // View bindings
  assignees: string[];
  listValues: string[];
  listClicked = false;

  // Score card numbers ( Note: using binding instead due to dc.NumberDisplay value bug )
  total = {
    open: 0,
    escalated: 0,
    reviewed: 0,
    closed: 0
  };

  // Material ripple styles
  centered = false;

  crossfilterData: crossfilter.Crossfilter<d4Data>;

  // Color Mapping
  userColorMap = {};

  constructor(
    private route: RouterOutlet,
  ) {}

  ngOnInit() {
    // Data from resolver
    const data: Array<d4Data> = this.route.activatedRoute.snapshot.data.homeData;

    // Add to crossfilter
    this.crossfilterData = crossfilter(data);

    // Populate Angular components
    this.statusDimension = this.crossfilterData.dimension(d => d.STATUS);
    this.listDimension = this.crossfilterData.dimension(d => d.ALERT_ACTION_USER);
    this.updateView();
  }

  ngAfterViewInit() {
    // Assign refs
    this.scoreCardRefs = [this.scoreCardOpenRef, this.scoreCardEscalatedRef, this.scoreCardReviewedRef, this.scoreCardClosedRef];

    // Create charts
    this.pieChart = dc.pieChart(this.pieChartRef.nativeElement);
    this.compositeChart = dc.compositeChart(this.compositeChartRef.nativeElement);
    this.heatmapChart = new dc.HeatMap(this.heatmapChartRef.nativeElement);
    this.heatmapLegend = new dc.HeatMap(this.heatmapLegendRef.nativeElement);
    this.createCharts(this.crossfilterData);
  }

  createCharts(data) {
    // Composite chart
    this.compositeDimension = data.dimension(d => d.ALERT_ACTION_DATE);
    const max = Math.max.apply(null, this.compositeDimension.top(Infinity).map(e => e.ALERT_ACTION_DATE.getTime()));
    const min = Math.min.apply(null, this.compositeDimension.top(Infinity).map(e => e.ALERT_ACTION_DATE.getTime()));
    this.compositeChart
        .width(this.compositeChartRef.nativeElement.offsetWidth)
        .height(this.compositeChartRef.nativeElement.offsetHeight)
        .renderLabel(true)
        .x(d3.scaleTime().domain([min, max]))
        .dimension(this.compositeDimension)
        .on('filtered', (c: any) => (this.updateView(), c))
        .legend(dc.legend().x(40).y(10).itemHeight(8).itemWidth(120).gap(7).horizontal(true))
        .margins({left: 30, right: 20, top: 10, bottom: 20})
        .elasticY(true)
        .compose(this.composeCharts(data));

    this.compositeChart.legendables = () => {
      const ret = this.compositeChart.children().reduce((items, child) => {
        if (this.listClicked) {
          if (this.listValues.length > 0) {
            if (this.listValues.includes(child._groupName)) {
              items.push.apply(items, child.legendables());
            }
          } else {
            if (this.assignees.includes(child._groupName)) {
              items.push.apply(items, child.legendables());
            }
          }
        } else {
          if (this.assignees.includes(child._groupName)) {
            items.push.apply(items, child.legendables());
          }
        }
        return items;
      }, []);
      this.listClicked = false;
      return ret;
    };

    this.compositeChart.render();
    this.generateUserColorMap();

    // Heatmap chart
    this.heatmapDimension = data.dimension(d => [d.ALERT_ACTION_DATE.getHours(), d.ALERT_ACTION_DATE.getDay()]);
    const heatmapGroup = this.heatmapDimension.group().reduceCount();

    const minValue = Math.min(...heatmapGroup.all().map(d => parseInt(d.value.toString(), 10)));
    const maxValue = Math.max(...heatmapGroup.all().map(d => parseInt(d.value.toString(), 10)));

    this.heatmapChart
        .width(this.heatmapChartRef.nativeElement.offsetWidth)
        .height(this.heatmapChartRef.nativeElement.offsetHeight)
        .dimension(this.heatmapDimension)
        .group(heatmapGroup)
        .keyAccessor(d => d.key[0])
        .valueAccessor(d => d.key[1])
        .rowsLabel(d => this.days[d])
        .colorAccessor(d => d.value)
        .on('preRedraw', chart => {
          chart.calculateColorDomain();
        })
        .colors(d3.scaleLinear().domain([minValue, maxValue])
          .interpolate(d3.interpolateHcl)
          .range([d3.rgb('#f7eac8'), d3.rgb('#b87272')])
        )
        .title(d => `Day: ${this.days[d.key[1]]}\nHour: ${d.key[0]}\nCount: ${d.value}`)
        .margins({left: 55, top: 0, right: 0,  bottom: 20})
        .on('filtered', (c: any) => (this.updateView(), c));

    this.heatmapChart.rowOrdering((a, b) => {
      if (this.daysOrder[this.days[a]] < this.daysOrder[this.days[b]]) { return 1; }
      if (this.daysOrder[this.days[a]] > this.daysOrder[this.days[b]]) { return -1; }
      return 0;
    });
    this.heatmapChart.render();

    // Heatmap legend
    const range = maxValue - minValue;
    const boxes = 24; // Hours in day
    const lookup = {};
    for (let i = 0; i < boxes; i++) {
      if (i === 0) {
        lookup[i] = minValue;
      } else if (i === (boxes - 1)) {
        lookup[i] = maxValue;
      } else {
        lookup[i] = (range / boxes) * i;
      }
    }

    this.heatmapLegendDimension = data.dimension(d => [lookup[d.ALERT_ACTION_DATE.getHours()], 1]);
    const heatmapLegendGroup = this.heatmapLegendDimension.group();
    this.heatmapLegend
        .width(this.heatmapLegendRef.nativeElement.offsetWidth)
        .height(this.heatmapLegendRef.nativeElement.offsetHeight)
        .dimension(this.heatmapLegendDimension)
        .group(heatmapLegendGroup)
        .keyAccessor(d => d.key[0])
        .valueAccessor(d => d.key[1])
        .on('preRedraw', chart => {
          chart.calculateColorDomain();
        })
        .colors(d3.scaleLinear().domain([minValue, maxValue])
          .interpolate(d3.interpolateHcl)
          .range(
            [d3.rgb('#f7eac8'), d3.rgb('#b87272')])
        )
        .rowsLabel(() => 'Legend')
        .colsLabel((d, i) => i % 2 === 1 ? '' : `${d3.format('.0f')(d)}`)
        .margins({left: 55, top: 0, right: 0,  bottom: 20});

    this.heatmapLegend.xBorderRadius(0);
    this.heatmapLegend.yBorderRadius(0);
    this.heatmapLegend.render();

    // Pie chart
    this.statusTypeDimension = data.dimension(d => d.STATUS_TYPE);
    const statusTypeGroup = this.statusTypeDimension.group().reduceCount();
    this.pieChart
        .width(this.pieChartRef.nativeElement.offsetWidth)
        .height(this.pieChartRef.nativeElement.offsetHeight)
        .dimension(this.statusTypeDimension)
        .group(statusTypeGroup)
        .innerRadius(65)
        .on('filtered', (c: any) => {
          this.filters = c._filters;
          this.updateView();
          return c;
        })
        .ordinalColors(['#A5C2AB', '#b87272', '#92aac4'])
        .on('pretransition', chart => {
          chart.selectAll('text.pie-slice').text(d => {
              const percent = dc.utils.printSingleValue((d.endAngle - d.startAngle) / (2 * Math.PI) * 100);
              return percent ? d.data.key + ' ' + percent + '%' : '';
          });
        })
        .render();
  }

  composeCharts(data): Array<any> {
    const chartsToCreate = [];
    const users = data.all().map(d => {
      if (this.filters.length === 0 || this.filters.includes(d.STATUS)) {
        return {user: d.ALERT_ACTION_USER, count: 1};
      }
    }).filter(d => d);
    const userObj = {};
    for (const e of users) {
      const name  = e.user;
      if (!userObj[name]) {
        userObj[name] = 0;
      }
      userObj[name] += 1;
    }
    const userCount = [];
    for (const obj of Object.keys(userObj)) {
      userCount.push([obj, userObj[obj]]);
    }
    userCount.sort((a, b) => {
      return b[1] - a[1];
    });
    const topUsers = userCount.map(d => d[0]);
    topUsers.forEach((u, i) => {
        chartsToCreate.push(
          dc.lineChart(this.compositeChart)
            .group(this.compositeDimension.group()
            .reduceSum(rs => rs.ALERT_ACTION_USER === u ? 1 : null), u)
            .curve(d3.curveBundle.beta(0.85))
            // Need to map color to user
            // .colors(['#' + Math.random().toString(16).slice(-6)])
            .colors([this.getColour()])
        );
    });
    return chartsToCreate;
  }


  getColour() {
    const hBase = Math.random();
    const newH = Math.floor(hBase * 360);
    const newS = 25 + 20 * Math.random();
    const newL = Math.floor(Math.random() * 15) + 65;
    return `hsl(${newH}, ${newS}%, ${newL}%)`;
  }
  updateView(): void {
    this.updateAssignees();
    this.updateScoreCards();
  }

  updateScoreCards(): void {
    this.total.open = this.crossfilterData.allFiltered().reduce((a,c) => a += (c.STATUS === 'open') ? 1 : 0, 0);
    this.total.escalated = this.crossfilterData.allFiltered().reduce((a,c) => a += (!this.notEscalatedStatus.includes(c.STATUS)) ? 1 : 0, 0);
    this.total.reviewed = this.crossfilterData.allFiltered().reduce((a,c) => a += (c.STATUS === 'reviewed') ? 1 : 0, 0);
    this.total.closed = this.crossfilterData.allFiltered().reduce((a,c) => a += (c.STATUS === 'closed') ? 1 : 0, 0);
  }

  updateAssignees(): void {
    if (!this.crossfilterData) {
      this.assignees = [];
      return;
    }
    const filteredAssignees: string[] = this.crossfilterData.allFiltered().reduce((a,c) => (a.push(c.ASSIGNEE), a), []);
    const uniqueAssignees = [...new Set(filteredAssignees)];
    this.assignees = uniqueAssignees.sort();
  }

  selectionChange(selection: MatSelectionListChange, redrawAll: boolean = false): void {
    this.listValues = selection.option.selectionList._value; // as unknown as d4Data[];
    if (this.listValues.length === 0 ) {
      this.listDimension.filterAll();
    } else {
      this.listDimension.filterFunction(value => this.listValues.includes(value));
    }
    // update all connected charts
    this.listClicked = true;
    if (redrawAll) {
      dc.redrawAll();
      this.updateScoreCards();
    }
  }

  clickStatus(event: MouseEvent, selectedValue: string): void {
    if (!selectedValue || selectedValue.length === 0) return;
    const target = event.currentTarget as HTMLElement;
    target.classList.toggle('selected');
    if (!target) {
      console.warn("Failed to get click element");
    }
    console.log('status selectedValue', selectedValue);
    // toggle on/off
    if (this.lastStatusSelected && this.lastStatusSelected === selectedValue) {
      this.listDimension.filterAll();
    } else {
      // update dimension filter
      switch(selectedValue) {
        case 'escalate':
        case 'escalated':
          console.log('escalate status not: ', this.notEscalatedStatus);
          this.statusDimension.filterFunction(value => !this.notEscalatedStatus.includes(value));
          break;
        default:
          this.statusDimension.filterFunction(value => selectedValue === value);
      }
    }

    // update all connected charts
    dc.redrawAll();
    // update other angular components
    this.updateAssignees();
    //
    this.lastStatusSelected = selectedValue;
  }

  reset(): void {
    console.log("reset");
    // reset filters
    this.filters = [];
    const dimensions: crossfilter.Dimension<any, any>[] = [
      this.listDimension,
      this.compositeDimension,
      this.statusTypeDimension,
      this.heatmapDimension,
      this.statusDimension,
    ];
    for (const dimension of dimensions) {
      dimension.filterAll();
    }

    // reset all charts
    dc.filterAll();
    dc.redrawAll();

    // deselect all list items
    this.listRef.deselectAll();

    // deselect all score cards
    this.scoreCardRefs.forEach(ref => {
      ref.nativeElement.classList.remove('selected');
    });

    // reset angular components
    this.updateView();
  }

  export(): void {
    const dimension = this.crossfilterData.dimension(x => true);
    const content = d3.csvFormat(dimension.top(Infinity));

    if (content.length === 0) {
      alert('Please reset filters or widen selection to include some data');
      return;
    }

    console.log('Export', content);
    this.download(content, 'export.csv', 'text/csv');
  }

  download(content: BlobPart, fileName: string, contentType: string): void {
    const a = document.createElement('a');
    const file = new Blob([content], {type: contentType});
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
  }

  generateUserColorMap() {
    setTimeout(() => {
      this.compositeChart.legendables().forEach(l => {
        this.userColorMap[l.name] = l.color;
      });
    }, 0);
  }

}
