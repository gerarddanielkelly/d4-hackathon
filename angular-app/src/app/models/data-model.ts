// tslint:disable-next-line: class-name
export interface d4Interface {
    ALERT_ACTION_DATE: string;
    STATUS: string;
    STATUS_TYPE: number;
    ALERT_ACTION_USER: string;
    ASSIGNEE: string;
    OWNER: string;
    RESOLVED: boolean;
}

// tslint:disable-next-line: class-name
export class d4Data {
    ALERT_ACTION_DATE: Date;
    STATUS: string;
    STATUS_TYPE: number;
    ALERT_ACTION_USER: string;
    ASSIGNEE: string;
    OWNER: string;
    RESOLVED: boolean;

    constructor(data: d4Interface) {
        this.ALERT_ACTION_DATE = new Date(data.ALERT_ACTION_DATE);
        this.STATUS = data.STATUS;
        this.STATUS_TYPE = data.STATUS_TYPE;
        this.ALERT_ACTION_USER = data.ALERT_ACTION_USER;
        this.ASSIGNEE = data.ASSIGNEE;
        this.OWNER = data.OWNER;
        this.RESOLVED = data.RESOLVED;
    }
}


