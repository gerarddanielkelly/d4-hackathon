import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { d4Data, d4Interface } from '../models/data-model';

@Injectable()
export class HomeResolver implements Resolve<Array<d4Data>> {
    constructor(
        protected httpClient: HttpClient
    ) { }

    resolve(): Observable<Array<d4Data>> {
        return this.httpClient.get<Array<d4Interface>>(`/assets/test-data.json`).pipe(
            map(resp => resp.map(data => new d4Data(data))),
            catchError(error => {
                console.log(error);
                return of([]);
            })
        );
    }
}
