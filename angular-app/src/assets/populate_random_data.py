import json
from random import randint


def create_message(days, hours_len):
  actioner = names[randint(0, 22)]
  status = statuses[randint(0, 20)]
  if status in ["news", "spam"]:
    status_type = "bad"
    resolved = 0
  elif status in ["open", "reviewed", "follow-up"]:
    status_type = "unknown"
    resolved = 1
  else:
    status_type = "good"
    resolved = 0
  return {
    "MESSAGE_ID": "1813-MES-4a7ad6a0eae34bb7-a19bf21400000000",
    "ALERT_ACTION_USER": actioner,
    "ASSIGNEE": names[randint(0, 22)],
    "OWNER": actioner,
    "STATUS": status,
    "STATUS_TYPE": status_type,
    "ALERT_ACTION_DATE": "2019-01-{} {}:01:01".format(days[randint(0, len(days)-1)], workHours[randint(0, hours_len)]),
    "RESOLVED": resolved
  }


mondays = ["06", "13", "20", "27"]
tuesdays = ["07", "14", "21", "28"]
wednesdays = ["01", "08", "15", "22", "29"]
thursdays = ["02", "09", "16", "23", "30"]
fridays = ["03", "10", "17", "24", "31"]
weekends = ["04", "05", "11", "12", "18", "19", "25", "26"]

workHours = ["08", "09", "09", "10", "10", "10", "11", "11", "13", "13", "13", "14", "14", "14", "15", "15", "15", "15",
             "16", "16", "16", "17", "17", "18"]

statuses = ["spam", "spam", "spam", "spam", "open", "open", "news", "news", "news", "closed", "closed", "closed",
            "escalate", "escalate", "follow-up", "follow-up", "reviewed", "reviewed", "breach", "breach", "breach"]

names = ["admin", "monitoring_admin", "naomi.hilliard", "charlie.penner", "krzysztof.zienkiewicz", "monitoring_user",
         "analyst", "clark.modesitt", "ben.lechlitner", "joseph.king", "sarah.cannon", "keith.massey", "suzy.holmes",
         "emily.maffett", "daniel.newman", "mary.roberts", "neha.wadhwa", "pete.scott", "priya.chaurasia",
         "clark.perkins", "andrew.middleton", "jerad.hobgood", "theo.carnavos"]

data = []

# Monday
for i in range(0, 4500, 1):
    data.append(create_message(mondays, 23))

# Tuesday
for i in range(0, 6500, 1):
    data.append(create_message(tuesdays, 23))

# Wednesday
for i in range(0, 6000, 1):
    data.append(create_message(wednesdays, 23))

# Thursday
for i in range(0, 5000, 1):
    data.append(create_message(thursdays, 23))

# Friday
for i in range(0, 3000, 1):
    data.append(create_message(fridays, 21))

# Suspicious Behavior
for i in range(0, 40, 1):
    message = {
        "MESSAGE_ID": "1813-MES-4a7ad6a0eae34bb7-a19bf21400000000",
        "ALERT_ACTION_USER": "krzysztof.zienkiewicz",
        "ASSIGNEE": "krzysztof.zienkiewicz",
        "OWNER": "krzysztof.zienkiewicz",
        "STATUS": "closed",
        "STATUS_TYPE": "good",
        "ALERT_ACTION_DATE": "2019-01-{} {}:01:01".format(weekends[randint(0, 7)], randint(21, 22)),
        "RESOLVED": 0
    }
    data.append(message)

with open('test-data.json', 'w') as outfile:
    json.dump(data, outfile)
